package com.example.employeemanagementsystem.controller;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.employeemanagementsystem.dto.ResponseDto;
import com.example.employeemanagementsystem.service.EmployeeService;

import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.PastOrPresent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/swipedata/employees")
public class EmployeeController {

	private final EmployeeService employeeService;

	@GetMapping("/{employeeId}")
	public ResponseEntity<ResponseDto> searchemployeeById(@PathVariable Long employeeId,@RequestParam Long admin,
			@RequestParam @PastOrPresent @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDate fromDate,
			@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDate toDate) {
		log.info("Employee controller - searchemployeeById");
		ResponseDto responseDto = employeeService.searchemployeeById(employeeId,admin,fromDate,toDate);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

}
