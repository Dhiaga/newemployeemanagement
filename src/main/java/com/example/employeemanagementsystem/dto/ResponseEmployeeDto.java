package com.example.employeemanagementsystem.dto;

import java.time.LocalDateTime;

import com.example.employeemanagementsystem.entity.SwipeType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseEmployeeDto {

	private Long employeeId;
	private SwipeType swipeType;
	private LocalDateTime localDateTime;

}
