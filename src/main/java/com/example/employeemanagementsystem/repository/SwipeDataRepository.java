package com.example.employeemanagementsystem.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.employeemanagementsystem.entity.Employee;
import com.example.employeemanagementsystem.entity.SwipeData;

@Repository
public interface SwipeDataRepository extends JpaRepository<SwipeData, Long> {

	List<SwipeData> findByEmployeeIdAndLocalDateTimeBetween(Employee employee, LocalDateTime start, LocalDateTime end);

}
