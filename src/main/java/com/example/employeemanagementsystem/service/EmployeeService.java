package com.example.employeemanagementsystem.service;

import java.time.LocalDate;

import com.example.employeemanagementsystem.dto.ResponseDto;

public interface EmployeeService {

	//ResponseDto searchemployeeById(Long employeeId, LocalDate date);

	//ResponseDto searchemployeeById(Long employeeId, LocalDate fromDate, LocalDate toDate);

	ResponseDto searchemployeeById(Long employeeId, Long employeeId1, LocalDate fromDate, LocalDate toDate);

}
