package com.example.employeemanagementsystem.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.employeemanagementsystem.dto.ResponseDto;
import com.example.employeemanagementsystem.dto.ResponseEmployeeDto;
import com.example.employeemanagementsystem.entity.Employee;
import com.example.employeemanagementsystem.entity.Role;
import com.example.employeemanagementsystem.entity.SwipeData;
import com.example.employeemanagementsystem.exception.Invaliddetailsexception;
import com.example.employeemanagementsystem.repository.EmployeeRepository;
import com.example.employeemanagementsystem.repository.SwipeDataRepository;
import com.example.employeemanagementsystem.utils.Response;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

	private final EmployeeRepository employeeRepository;
	private final SwipeDataRepository swipeDataRepository;

	@Override
	public ResponseDto searchemployeeById(Long employeeId,Long admin, LocalDate fromDate, LocalDate toDate) {

		Employee employee1 = employeeRepository.findById(admin)
				.orElseThrow(() -> new Invaliddetailsexception(Response.ERROR_MESSAGE_INVALID_EMPLOYEE_ID));
		
		if (!employee1.getRole().equals(Role.ADMIN)) {
			throw new Invaliddetailsexception(Response.ERROR_MESSAGE_FORBIDDEN_ACCESS);
		}
	
		
		Employee employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new Invaliddetailsexception(Response.ERROR_MESSAGE_INVALID_EMPLOYEE_ID));

		
		
		LocalDateTime start = fromDate.atStartOfDay();
		LocalDateTime end = toDate.atTime(LocalTime.MAX);
		
		if(start.isAfter(end)) {
			throw new Invaliddetailsexception(Response.ERROR_MESSAGE_INVALID_DATE_FORMAT);
		}


		List<SwipeData> swipeData = swipeDataRepository.findByEmployeeIdAndLocalDateTimeBetween(employee, start, end);
		if(swipeData.isEmpty()) {
			throw new Invaliddetailsexception(Response.ERROR_MESSAGE_NO_DATA);
		}
		List<ResponseEmployeeDto> responseEmployeeDtolist = new ArrayList<>();
		swipeData.forEach(swipe -> {
			ResponseEmployeeDto responseEmployeeDto = new ResponseEmployeeDto();
			responseEmployeeDto.setEmployeeId(employee.getEmployeeId());
			responseEmployeeDto.setSwipeType(swipe.getSwipeType());
			responseEmployeeDto.setLocalDateTime(swipe.getLocalDateTime());
			responseEmployeeDtolist.add(responseEmployeeDto);
		});
		ResponseDto responseDto = new ResponseDto();
		responseDto.setStatusCode(200L);
		responseDto.setStatusMessage(Response.SUCCESS_CODE);
		responseDto.setData(responseEmployeeDtolist);

		return responseDto;
	}

}
