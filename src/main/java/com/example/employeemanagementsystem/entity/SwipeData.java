package com.example.employeemanagementsystem.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SwipeData {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long swipeId;
	@ManyToOne
	@JoinColumn(name = "employeeId")
	private Employee employeeId;
	@Enumerated(EnumType.STRING)
	private SwipeType swipeType;
	private LocalDateTime localDateTime;
	

}
