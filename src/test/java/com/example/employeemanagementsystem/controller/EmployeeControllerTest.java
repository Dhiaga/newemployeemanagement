package com.example.employeemanagementsystem.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.employeemanagementsystem.dto.ResponseDto;
import com.example.employeemanagementsystem.dto.ResponseEmployeeDto;
import com.example.employeemanagementsystem.entity.SwipeType;
import com.example.employeemanagementsystem.service.EmployeeServiceImpl;
import com.example.employeemanagementsystem.utils.Response;

@SpringBootTest
public class EmployeeControllerTest {

	@Mock
	EmployeeServiceImpl employeeServiceImpl;

	@InjectMocks
	EmployeeController employeeController;

	@Test
	void testsearchemployee() {
		ResponseEmployeeDto responseEmployeeDto = new ResponseEmployeeDto(1l, SwipeType.IN,
				LocalDateTime.of(2024, 4, 8, 8, 35));
		ResponseDto responseDto = new ResponseDto(2000l, Response.SUCCESS_CODE, responseEmployeeDto);
		when(employeeServiceImpl.searchemployeeById(1l, 1l, LocalDate.of(2024, 4, 1), LocalDate.of(2024, 5, 20)))
				.thenReturn(responseDto);
		ResponseEntity<?> response = employeeController.searchemployeeById(1l, 1l, LocalDate.of(2024, 4, 1),
				LocalDate.of(2024, 5, 20));
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

}
