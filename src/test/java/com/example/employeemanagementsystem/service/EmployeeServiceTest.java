package com.example.employeemanagementsystem.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.employeemanagementsystem.dto.ResponseDto;
import com.example.employeemanagementsystem.dto.ResponseEmployeeDto;
import com.example.employeemanagementsystem.entity.Department;
import com.example.employeemanagementsystem.entity.Employee;
import com.example.employeemanagementsystem.entity.Role;
import com.example.employeemanagementsystem.entity.SwipeData;
import com.example.employeemanagementsystem.entity.SwipeType;
import com.example.employeemanagementsystem.exception.Invaliddetailsexception;
import com.example.employeemanagementsystem.repository.EmployeeRepository;
import com.example.employeemanagementsystem.repository.SwipeDataRepository;
import com.example.employeemanagementsystem.utils.Response;

@SpringBootTest
public class EmployeeServiceTest {

	@Mock
	EmployeeRepository employeeRepository;

	@Mock
	SwipeDataRepository swipeDataRepository;

	@InjectMocks
	EmployeeServiceImpl employeeServiceImpl;

	@Test
	void testsearchemployee() {
		List<ResponseEmployeeDto> list = new ArrayList<>();
		ResponseEmployeeDto responseEmployeeDto = new ResponseEmployeeDto(1l, SwipeType.IN,
				LocalDateTime.of(2024, 4, 8, 8, 35));
		list.add(responseEmployeeDto);
		ResponseDto responseDto = new ResponseDto(2000l, Response.SUCCESS_CODE, responseEmployeeDto);
		Employee employee1 = new Employee(1l, "Vijay", "abc@gmail.com", Department.DEV, 9876543210l, Role.ADMIN);
		LocalDateTime start = LocalDate.now().atStartOfDay();
		LocalDateTime end = LocalDate.now().atTime(LocalTime.MAX);
		List<SwipeData> swipeData = new ArrayList<>();
		SwipeData swipeData1 = new SwipeData(1l, employee1, SwipeType.IN, LocalDateTime.of(2024, 4, 8, 8, 30));
		swipeData.add(swipeData1);
		when(employeeRepository.findById(1l)).thenReturn(Optional.of(employee1));
		when(swipeDataRepository.findByEmployeeIdAndLocalDateTimeBetween(employee1, start, end)).thenReturn(swipeData);
		ResponseDto response = employeeServiceImpl.searchemployeeById(1l, 1l, LocalDate.of(2024, 4, 8),
				LocalDate.of(2024, 4, 8));

		assertEquals(Response.SUCCESS_CODE, response.getStatusMessage());
	}
	
	@Test
	void testsearchemployeeInvalidId() {
		List<ResponseEmployeeDto> list = new ArrayList<>();
		ResponseEmployeeDto responseEmployeeDto = new ResponseEmployeeDto(1l, SwipeType.IN,
				LocalDateTime.of(2024, 4, 8, 8, 35));
		list.add(responseEmployeeDto);
		ResponseDto responseDto = new ResponseDto(2000l, Response.SUCCESS_CODE, responseEmployeeDto);
		Employee employee1 = new Employee(1l, "Vijay", "abc@gmail.com", Department.DEV, 9876543210l, Role.ADMIN);
		LocalDateTime start = LocalDate.now().atStartOfDay();
		LocalDateTime end = LocalDate.now().atTime(LocalTime.MAX);
		List<SwipeData> swipeData = new ArrayList<>();
		SwipeData swipeData1 = new SwipeData(1l, employee1, SwipeType.IN, LocalDateTime.of(2024, 4, 8, 8, 30));
		swipeData.add(swipeData1);
		when(employeeRepository.findById(1l)).thenReturn(Optional.empty());
		when(swipeDataRepository.findByEmployeeIdAndLocalDateTimeBetween(employee1, start, end)).thenReturn(swipeData);
	   
		Invaliddetailsexception exception = assertThrows(Invaliddetailsexception.class,()-> employeeServiceImpl.searchemployeeById(1l, 1l, LocalDate.of(2024, 4, 8),
				LocalDate.of(2024, 4, 8)));
		assertEquals(Response.ERROR_MESSAGE_INVALID_EMPLOYEE_ID,exception.getMessage());
	}
	
	@Test
	void testsearchemployeeNoData() {
		List<ResponseEmployeeDto> list = new ArrayList<>();
		ResponseEmployeeDto responseEmployeeDto = new ResponseEmployeeDto(1l, SwipeType.IN,
				LocalDateTime.of(2024, 4, 8, 8, 35));
		list.add(responseEmployeeDto);
		ResponseDto responseDto = new ResponseDto(2000l, Response.SUCCESS_CODE, responseEmployeeDto);
		Employee employee1 = new Employee(1l, "Vijay", "abc@gmail.com", Department.DEV, 9876543210l, Role.ADMIN);
		LocalDateTime start = LocalDate.now().atStartOfDay();
		LocalDateTime end = LocalDate.now().atTime(LocalTime.MAX);
		List<SwipeData> swipeData = new ArrayList<>();
		SwipeData swipeData1 = new SwipeData(1l, employee1, SwipeType.IN, LocalDateTime.of(2024, 4, 8, 8, 30));
		swipeData.add(swipeData1);
		when(employeeRepository.findById(1l)).thenReturn(Optional.of(employee1));
		when(swipeDataRepository.findByEmployeeIdAndLocalDateTimeBetween(employee1, start, end)).thenReturn(swipeData);
	   
		Invaliddetailsexception exception = assertThrows(Invaliddetailsexception.class,()-> employeeServiceImpl.searchemployeeById(1l, 1l, LocalDate.of(2024, 4, 8),
				LocalDate.of(2024, 4, 8)));
		assertEquals(Response.ERROR_MESSAGE_NO_DATA,exception.getMessage());
	}

}
